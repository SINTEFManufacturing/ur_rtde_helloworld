__author__ = "Eirik Njaastad"
__copyright__ = "SINTEF Manufacturing 2023"
__credits__ = ["Eirik Njaastad"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njaastad"
__email__ = "eirik.njaastad@sintef.no"
__status__ = "Development"

"""
- The robot IP is found under File->About in the teach-pendant menus
- To use the RDEControlInterface, make sure the RTDE input registers are not
  already in use by disabling the EtherNet/IP adapter, PROFINET or any MODBUS
  unit configured on the robot.
"""
import math
import time

import rtde_control
import rtde_receive

robot_ip = "192.168.0.90"

rtde_r = rtde_receive.RTDEReceiveInterface(robot_ip)
rtde_c = rtde_control.RTDEControlInterface(robot_ip)

if rtde_r.isConnected():
    print(f"Connected to robot on {robot_ip}!")

# Get the initial robot pose
actual_pose = rtde_r.getActualTCPPose()

# Define the sine wave parameters
amplitude = 0.1  # Amplitude of the sine wave in meters
frequency = 0.4  # Frequency of the sine wave in Hz
offset = actual_pose[2]  # Start the sine motion from current Z-value

# Define the control and motion parameters
speed = 0.5
acceleration = 0.5
lookahead_time = 0.1
dt = 0.02
gain = 300

# Main loop
while True:
    # Get the current robot time
    t = rtde_r.getTimestamp()

    # Calculate the desired Z-coordinate using the sine wave equation
    desired_z = offset + amplitude * math.sin(2 * math.pi * frequency * t)

    t_start = rtde_c.initPeriod()
    # Get the current Cartesian pose of the robot
    actual_pose = rtde_r.getActualTCPPose()

    # Set the desired Z-coordinate of the pose
    desired_pose = actual_pose
    desired_pose[2] = desired_z

    # Move the robot to the desired pose using Cartesian linear motion
    rtde_c.servoL(desired_pose, speed, acceleration, dt, lookahead_time, gain)

    rtde_c.waitPeriod(t_start)

# Disconnect from the robot
rtde_c.stopScript()

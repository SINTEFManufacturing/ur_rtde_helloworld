__author__ = "Eirik Njaastad"
__copyright__ = "SINTEF Manufacturing 2023"
__credits__ = ["Eirik Njaastad"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njaastad"
__email__ = "eirik.njaastad@sintef.no"
__status__ = "Development"

"""
- The robot IP is found under File->About in the teach-pendant menus
- To use the RDEControlInterface, make sure the RTDE input registers are not
  already in use by disabling the EtherNet/IP adapter, PROFINET or any MODBUS
  unit configured on the robot.
"""
import math

import rtde_control
import rtde_receive

robot_ip = "192.168.0.90"

rtde_r = rtde_receive.RTDEReceiveInterface(robot_ip)
rtde_c = rtde_control.RTDEControlInterface(robot_ip)

if rtde_r.isConnected():
    print(f"Connected to robot on {robot_ip}!")


frequency = 0.2  # Frequency of the sine wave in Hz
max_velocity = 0.05  # Maximum velocity in m/s

# Main loop
while True:
    # Get the current robot time
    t = rtde_r.getTimestamp()

    # Calculate the desired velocity based on the desired Z-coordinate
    desired_velocity = max_velocity * math.cos(2 * math.pi * frequency * t)

    # # Calculate the desired Cartesian velocity vector
    desired_velocity_vector = [0, 0, desired_velocity, 0, 0, 0]

    # Move the robot with the desired Cartesian velocity
    rtde_c.speedL(desired_velocity_vector, 0.1)

# Disconnect from the robot
rtde_c.stopScript()

# ur_rtde_helloworld

## Description
A simple package showing how to use ur_rtde for controlling UR robots through Python.

The only required dependency is the ur_rtde from SDU: https://sdurobotics.gitlab.io/ur_rtde/


## Installation
Not ment for installation, but package is set up using Poetry: https://python-poetry.org/

## Usage
There are two example scripts provided in the folder "ur_rtde_helloworld":
 - position control of the robot (sinus_position_example.py), and
 - velocity control of the robot (sinus_velocity_example.py)

## Support
You can ask Eirik: eirik.njaastad@sintef.no

## Author
Made by Eirik B. Njåstad, SINTEF Manufacturing in 2023.

## License
GPLv3
